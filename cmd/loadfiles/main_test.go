package main

import (
	// "os"
	"testing"
)

const testFile = "/mnt/c/Users/peza/OneDrive/CBS/FinOps/Detail_Enrollment_83856065_202009_en.csv"

var (
	testOpts = options{
		inputFiles: []string{
			testFile,
		},
		dbHost:    "localhost",
		dbName:    "azurecosts",
		tablename: "azurecosts",
		password:  "password",
		prompt:    true,
		user:      "admin",
	}

	testConnectionargs = []string{
		"programname",
		"--user", "peza",
		"--input", "filename",
		"--input", "filename1",
		"--password", "secret",
	}

	loadFilesArgs = []string{
		"programname",
		"--user", "peza",
		"--input", testFile,
		"--password", "secret",
	}
)

func TestOpenDB(t *testing.T) {
	opts := &options{}
	opts.Parse(testConnectionargs)
	// Open the database.
	db, err := openDB(opts)
	if err != nil {
		t.Fatalf("error opening DB: %s", err)
	}
	err = testConnection(db)
	if err != nil {
		t.Fatalf("cannot connect to the DB: %s", err)
	}
}

func TestLoadFile(t *testing.T) {
	opts := &options{}
	opts.Parse(loadFilesArgs)
	db, err := openDB(opts)
	if err != nil {
		t.Fatalf("error opening DB: %s", err)
	}
	defer func() {
		_ = db.Close()
	}()
	DefaultConfig.db = db
	err = testConnection(db)
	if err != nil {
		t.Fatalf("cannot connect to the DB: %s", err)
	}
	err = loadFile(opts, opts.inputFiles[0])
	if err != nil {
		t.Fatalf("error loading files: %s", err)
	}
}
