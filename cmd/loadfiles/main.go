package main

import (
	"bytes"
	"database/sql"
	"database/sql/driver"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/spf13/pflag"
	"golang.org/x/crypto/ssh/terminal"

	_ "github.com/lib/pq"
)

const (
	filename = "/mnt/c/Users/peza/OneDrive/CBS/FinOps/Detail_Enrollment_83856065_202009_en.csv"
)

func createDatabase() {

}

func prepareRecord() {

}

type converterFunc func(string) interface{}

// Converters
type converter struct {
	name string
	conv converterFunc
}

func stringConverter(val string) interface{} {
	return val
}

func booleanConverter(val string) interface{} {
	return strings.ToLower(val) == "true"
}

type JSONField map[string]interface{}

func (jf JSONField) Value() (driver.Value, error) {
	return json.Marshal(jf)
}

func jsonConverter(val string) interface{} {
	var j JSONField
	if err := json.Unmarshal([]byte(val), &j); err != nil {
		return nil
	}
	return j
}

func newDateConverter(format string) converterFunc {
	return func(val string) interface{} {
		res, err := time.Parse(format, val)
		if err != nil {
			return nil
		}
		return res
	}
}

// formaat mm/dd/
var (
	azureDateConverter = newDateConverter("01/02/2006")
)

var converters = []converter{
	{conv: stringConverter, name: "billingaccountid"},
	{conv: stringConverter, name: "billingaccountname"},
	{conv: azureDateConverter, name: "billingperiodstartdate"},
	{conv: azureDateConverter, name: "billingperiodenddate"},
	{conv: stringConverter, name: "billingprofileid"},
	{conv: stringConverter, name: "billingprofilename"},
	{conv: stringConverter, name: "accountownerid"},
	{conv: stringConverter, name: "accountname"},
	{conv: stringConverter, name: "subscriptionid"},
	{conv: stringConverter, name: "subscriptionname"},
	{conv: azureDateConverter, name: "date"},
	{conv: stringConverter, name: "product"},
	{conv: stringConverter, name: "partnumber"},
	{conv: stringConverter, name: "meterid"},
	{conv: stringConverter, name: "servicefamily"},
	{conv: stringConverter, name: "metercategory"},
	{conv: stringConverter, name: "metersubcategory"},
	{conv: stringConverter, name: "meterregion"},
	{conv: stringConverter, name: "metername"},
	{conv: stringConverter, name: "quantity"},
	{conv: stringConverter, name: "effectiveprice"},
	{conv: stringConverter, name: "cost"},
	{conv: stringConverter, name: "unitprice"},
	{conv: stringConverter, name: "billingcurrency"},
	{conv: stringConverter, name: "resourcelocation"},
	{conv: stringConverter, name: "availabilityzone"},
	{conv: stringConverter, name: "consumedservice"},
	{conv: stringConverter, name: "resourceid"},
	{conv: stringConverter, name: "resourcename"},
	{conv: stringConverter, name: "serviceinfo1"},
	{conv: stringConverter, name: "serviceinfo2"},
	{conv: jsonConverter, name: "additionalinfo"},
	{conv: stringConverter, name: "tags"},
	{conv: stringConverter, name: "invoicesectionid"},
	{conv: stringConverter, name: "invoicesection"},
	{conv: stringConverter, name: "costcenter"},
	{conv: stringConverter, name: "unitofmeasure"},
	{conv: stringConverter, name: "resourcegroup"},
	{conv: stringConverter, name: "reservationid"},
	{conv: stringConverter, name: "reservationname"},
	{conv: stringConverter, name: "productorderid"},
	{conv: stringConverter, name: "productordername"},
	{conv: stringConverter, name: "offerid"},
	{conv: booleanConverter, name: "isazurecrediteligible"},
	{conv: stringConverter, name: "term"},
	{conv: stringConverter, name: "publishername"},
	{conv: stringConverter, name: "planname"},
	{conv: stringConverter, name: "chargetype"},
	{conv: stringConverter, name: "frequency"},
	{conv: stringConverter, name: "publishertype"},
	{conv: stringConverter, name: "paygprice"},
	{conv: stringConverter, name: "pricingmodel"},
}

const (
	billingaccountid = iota
	billingaccountname
	billingperiodstartdate
	billingperiodenddate
	billingprofileid
	billingprofilename
	accountownerid
	accountname
	subscriptionid
	subscriptionname
	date
	product
	partnumber
	meterid
	servicefamily
	metercategory
	metersubcategory
	meterregion
	metername
	quantity
	effectiveprice
	cost
	unitprice
	billingcurrency
	resourcelocation
	availabilityzone
	consumedservice
	resourceid
	resourcename
	serviceinfo1
	serviceinfo2
	additionalinfo
	tags
	invoicesectionid
	invoicesection
	costcenter
	unitofmeasure
	resourcegroup
	reservationid
	reservationname
	productorderid
	productordername
	offerid
	isazurecrediteligible
	term
	publishername
	planname
	chargetype
	frequency
	publishertype
	paygprice
	pricingmodel
)

const (
	insertAzureCostsSql = `
	INSERT INTO public.azurecosts(
		billingaccountid, billingaccountname, billingperiodstartdate, billingperiodenddate, billingprofileid, billingprofilename, accountownerid, accountname, subscriptionid, subscriptionname, date, product, partnumber, meterid, servicefamily, metercategory, metersubcategory, meterregion, metername, quantity, effectiveprice, cost, unitprice, billingcurrency, resourcelocation, availabilityzone, consumedservice, resourceid, resourcename, serviceinfo1, serviceinfo2, additionalinfo, tags, invoicesectionid, invoicesection, costcenter, unitofmeasure, resourcegroup, reservationid, reservationname, productorderid, productordername, offerid, isazurecrediteligible, term, publishername, planname, chargetype, frequency, publishertype, paygprice, pricingmodel)
		VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`

	insertAzureCostsSqlsmall = `
		INSERT INTO public.azurecosts(billingaccountid) VALUES (?);`
)

func buildInsertStatement(table string, cols []converter) string {
	w := &bytes.Buffer{}
	fmt.Fprintf(w, "insert into %s (", table)
	for i, col := range cols {
		if i > 0 {
			fmt.Fprintf(w, ",")
		}
		fmt.Fprintf(w, "%s", col.name)
	}
	fmt.Fprint(w, ") values (")
	for i := 0; i < len(cols); i++ {
		if i > 0 {
			fmt.Fprintf(w, ",")
		}
		fmt.Fprintf(w, "$%d", i+1)
	}
	fmt.Fprint(w, ");")
	return w.String()
}

func convertVals(rec []string) []interface{} {
	if len(rec) != len(converters) {
		log.Printf("unequal lengths for rec %d and converers %d", len(rec), len(converters))
	}
	vals := make([]interface{}, len(rec))
	for i := 0; i < len(rec); i++ {
		vals[i] = converters[i].conv(rec[i])
	}
	return vals
}

func putRecord(tx *sql.Tx, statement string, rec []string) error {
	vals := convertVals(rec)
	res, err := tx.Exec(statement, vals...)
	if err != nil {
		return err
	}
	_ = res
	return nil
}

// func putSmalRecord(rec []string) error {
// 	val := converters[0](rec[0])
// 	res, err := DefaultConfig.db.Exec(insertAzureCostsSqlsmall, val)
// 	if err != nil {
// 		return err
// 	}
// 	_ = res
// 	return nil
// }

func insertTestRecord() {
	stmt := "insert into public.azurecosts (billingaccountid) values ($1);"
	var val interface{} = "waarde"
	res, err := DefaultConfig.db.Exec(stmt, val)
	if err != nil {
		fmt.Printf("error inserting test record: %s\n", err)
	}
	_ = res
}

// func loadFile(r io.Reader, done <-chan struct{}) <-chan []string {
// 	out := make(chan []string)
// 	reader := csv.NewReader(r)

// 	go func() {
// 		for {
// 			record, err := reader.Read()
// 			if err != nil {
// 				close(out)
// 				return
// 			}
// 			select {
// 			case out <- record:
// 			case <-done:
// 				return
// 			}
// 		}
// 	}()
// 	return out
// }

func printStringArray(s []string) {
	for _, l := range s {
		fmt.Printf("    %s\n", l)
	}
}

func dumpRecords(recs <-chan []string) {

}

// func loadRecords(recs <-chan []string) error {
// 	var first = true
// 	stmt := buildInsertStatement("public.azurecosts", converters)
// 	for rec := range recs {
// 		if first {
// 			first = false
// 			continue
// 		}
// 		if err := putRecord(tx, stmt, rec); err != nil {
// 			return err
// 		}
// 	}
// 	return nil
// }

const (
	connStr = "user=peza password=password dbname=azurecosts host=localhost sslmode=disable"
	connFmt = "user='%s' password='%s' dbname='%s' host='%s' sslmode=disable"
)

func connectionString(user, password, dbname, host string) string {
	return fmt.Sprintf(connFmt, user, password, dbname, host)
}

type config struct {
	db *sql.DB
}

func testConnection(db *sql.DB) error {
	res, err := db.Query(`select * from public.azurecosts`)
	if err != nil {
		return err
	}
	_ = res
	return nil
}

var DefaultConfig config

type options struct {
	inputFiles []string
	dbHost     string
	dbName     string
	user       string
	password   string
	prompt     bool
	tablename  string
	statement  string
}

func (o *options) Parse(args []string) {
	f := pflag.NewFlagSet("default", pflag.ContinueOnError)
	f.StringArrayVarP(&o.inputFiles, "input", "i", []string{}, "names of the files to load")
	f.StringVarP(&o.dbHost, "host", "h", "localhost", "db server host")
	f.StringVarP(&o.dbName, "dbname", "d", "azurecosts", "name of the database")
	f.StringVarP(&o.tablename, "table", "t", "azurecosts", "name of the database table")
	f.StringVarP(&o.user, "user", "u", "peza", "username")
	f.StringVarP(&o.password, "password", "p", "", "password")
	f.BoolVarP(&o.prompt, "prompt", "P", false, "prompt for password")
	_ = f.Parse(args[1:])

	o.statement = buildInsertStatement(o.tablename, converters)
}

func loadFile(opts *options, file string) error {
	// Open file.
	f, err := os.Open(filepath.Clean(filename))
	if err != nil {
		fmt.Printf("error opening file: %s\n", err)
		return err
	}
	defer func() {
		_ = f.Close()
	}()
	r := csv.NewReader(f)
	// skip headers
	if _, err := r.Read(); err != nil {
		return err
	}
	var i int
	var tx *sql.Tx
	for rec, err := r.Read(); err == nil; rec, err = r.Read() {
		if i%1000 == 0 {
			if tx != nil {
				_ = tx.Commit()
			}
			tx, err = DefaultConfig.db.Begin()
			if err != nil {
				return err
			}
		}
		if err := putRecord(tx, opts.statement, rec); err != nil {
			fmt.Printf("put record error: %s\n", err)
		}
		i++
	}
	if tx != nil {
		_ = tx.Commit()
	}
	return nil
}

func loadFiles(opts *options) error {
	for _, file := range opts.inputFiles {
		err := loadFile(opts, file)
		if err != nil {
			fmt.Printf("error loading file %s: %s", file, err)
		}
	}
	return nil
}

func setPassword(opts *options) error {
	if !opts.prompt {
		return nil
	}
	fmt.Print("Password: ")
	b, err := terminal.ReadPassword(0)
	if err != nil {
		return err
	}
	fmt.Println()
	opts.password = string(b)
	return nil
}

func openDB(opts *options) (*sql.DB, error) {
	db, err := sql.Open("postgres",
		connectionString(
			opts.user,
			opts.password,
			opts.dbName,
			opts.dbHost,
		))
	if err != nil {
		return nil, err
	}
	return db, nil
}

func main() {
	options := &options{}
	options.Parse(os.Args)
	if len(options.inputFiles) == 0 {
		fmt.Println("input files missing, use --input per file.")
		return
	}

	// set password
	if err := setPassword(options); err != nil {
		return
	}

	// Open database.
	var err error
	DefaultConfig.db, err = openDB(options)
	if err != nil {
		fmt.Printf("error opening database: %s\n", err)
		return
	}
	// test the database
	if err := testConnection(DefaultConfig.db); err != nil {
		fmt.Printf("error connecting to the database: %s\n", err)
		return
	}
	defer DefaultConfig.db.Close()

	err = loadFiles(options)
	if err != nil {
		fmt.Printf("error loading files: %s", err)
		return
	}
	fmt.Printf("files loaded succesfully\n")
}
