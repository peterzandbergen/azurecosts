package main

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"sync"
	"time"

	"azurecosts/field"

	"github.com/spf13/pflag"
	"golang.org/x/crypto/ssh/terminal"

	_ "github.com/lib/pq"
)

// formaat mm/dd/
var (
	azureDateConverter = field.NewDateConverter("01/02/2006")
)

var converters = []field.Converter{
	{Conv: field.StringConverter, Name: "billingaccountid"},
	{Conv: field.StringConverter, Name: "billingaccountname"},
	{Conv: azureDateConverter, Name: "billingperiodstartdate"},
	{Conv: azureDateConverter, Name: "billingperiodenddate"},
	{Conv: field.StringConverter, Name: "billingprofileid"},
	{Conv: field.StringConverter, Name: "billingprofilename"},
	{Conv: field.StringConverter, Name: "accountownerid"},
	{Conv: field.StringConverter, Name: "accountname"},
	{Conv: field.StringConverter, Name: "subscriptionid"},
	{Conv: field.StringConverter, Name: "subscriptionname"},
	{Conv: azureDateConverter, Name: "date"},
	{Conv: field.StringConverter, Name: "product"},
	{Conv: field.StringConverter, Name: "partnumber"},
	{Conv: field.StringConverter, Name: "meterid"},
	{Conv: field.StringConverter, Name: "servicefamily"},
	{Conv: field.StringConverter, Name: "metercategory"},
	{Conv: field.StringConverter, Name: "metersubcategory"},
	{Conv: field.StringConverter, Name: "meterregion"},
	{Conv: field.StringConverter, Name: "metername"},
	{Conv: field.StringConverter, Name: "quantity"},
	{Conv: field.StringConverter, Name: "effectiveprice"},
	{Conv: field.StringConverter, Name: "cost"},
	{Conv: field.StringConverter, Name: "unitprice"},
	{Conv: field.StringConverter, Name: "billingcurrency"},
	{Conv: field.StringConverter, Name: "resourcelocation"},
	{Conv: field.StringConverter, Name: "availabilityzone"},
	{Conv: field.StringConverter, Name: "consumedservice"},
	{Conv: field.StringConverter, Name: "resourceid"},
	{Conv: field.StringConverter, Name: "resourcename"},
	{Conv: field.StringConverter, Name: "serviceinfo1"},
	{Conv: field.StringConverter, Name: "serviceinfo2"},
	{Conv: field.JSONConverter, Name: "additionalinfo"},
	{Conv: field.StringConverter, Name: "tags"},
	{Conv: field.StringConverter, Name: "invoicesectionid"},
	{Conv: field.StringConverter, Name: "invoicesection"},
	{Conv: field.StringConverter, Name: "costcenter"},
	{Conv: field.StringConverter, Name: "unitofmeasure"},
	{Conv: field.StringConverter, Name: "resourcegroup"},
	{Conv: field.StringConverter, Name: "reservationid"},
	{Conv: field.StringConverter, Name: "reservationname"},
	{Conv: field.StringConverter, Name: "productorderid"},
	{Conv: field.StringConverter, Name: "productordername"},
	{Conv: field.StringConverter, Name: "offerid"},
	{Conv: field.BooleanConverter, Name: "isazurecrediteligible"},
	{Conv: field.StringConverter, Name: "term"},
	{Conv: field.StringConverter, Name: "publishername"},
	{Conv: field.StringConverter, Name: "planname"},
	{Conv: field.StringConverter, Name: "chargetype"},
	{Conv: field.StringConverter, Name: "frequency"},
	{Conv: field.StringConverter, Name: "publishertype"},
	{Conv: field.StringConverter, Name: "paygprice"},
	{Conv: field.StringConverter, Name: "pricingmodel"},
}

func stringChan(ctx context.Context, str ...string) <-chan string {
	out := make(chan string, 1)
	go func() {
		var nMsg int
		log := contextLogger(ctx)
		for _, s := range str {
			select {
			case <-ctx.Done():
				log.Printf("stringChan: done closed")
				break
			case out <- s:
			}
			nMsg++
		}
		close(out)
		log.Printf("stringChan: end of goroutine, processed %d messages", nMsg)
	}()
	return out
}

// fileChan consumes file names and produces a ReadCloser.
func fileChan(ctx context.Context, names <-chan string) <-chan io.ReadCloser {
	out := make(chan io.ReadCloser, 1)
	go func() {
		var nMsg int
		log := contextLogger(ctx)
		defer close(out)
		for name := range names {
			f, err := os.Open(filepath.Clean(name))
			if err != nil {
				log.Printf("fileChan: error opening file: %s", err)
			}
			select {
			case out <- f:
			case <-ctx.Done():
				log.Printf("fileChan: done closed")
				break
			}
			nMsg++
		}
		log.Printf("fileChan: end of goroutine, processed %d message", nMsg)
	}()
	return out
}

// recordChanFromReader produces csv records from a channel of ReadClosers.
// It closes r when done.
func recordChanFromReader(ctx context.Context, rchan <-chan io.ReadCloser) <-chan []string {
	var nMsg int
	log := contextLogger(ctx)
	out := make(chan []string, 1)

	processReader := func(ctx context.Context, r io.ReadCloser) {
		var nRecs int
		defer r.Close()
		csvr := csv.NewReader(r)
		var isFirst = true
		for rec, err := csvr.Read(); err == nil; rec, err = csvr.Read() {
			if isFirst {
				isFirst = false
				continue
			}
			select {
			case <-ctx.Done():
				log.Printf("recordChanFromReader.processReader: done closed")
				break
			case out <- rec:
			}
			nRecs++
			nMsg++
		}
		log.Printf("recordChanFromReader.processReader: read %d records", nRecs)
	}

	go func() {
		defer close(out)
		for r := range rchan {
			log.Printf("recordChanFromReader: read reader from rchan")
			processReader(ctx, r)
			// Test if Done
			select {
			case <-ctx.Done():
				log.Printf("recordChanFromReader: done closed")
				break
			default:
			}
		}
		log.Printf("recordChanFromReader: end of goroutine, processed %d messages", nMsg)
	}()
	return out
}

func convertVals(rec []string) []interface{} {
	if len(rec) != len(converters) {
		log.Printf("unequal lengths for rec %d and Converers %d", len(rec), len(converters))
	}
	vals := make([]interface{}, len(rec))
	for i := 0; i < len(rec); i++ {
		vals[i] = converters[i].Conv(rec[i])
	}
	return vals
}

func convertRecord(ctx context.Context, recs <-chan []string) <-chan []interface{} {
	out := make(chan []interface{}, 1)
	// log := contextLogger(ctx)
	go func() {
		log := contextLogger(ctx)
		var nMsg int
		defer close(out)
		for rec := range recs {
			var res []interface{}
			res = convertVals(rec)

			select {
			case <-ctx.Done():
				log.Printf("convertRecord: done closed")
				break
			case out <- res:
			}
			nMsg++
		}
		log.Printf("convertRecord: end of goroutine, processed %d messages", nMsg)
	}()
	return out
}

// putRecords puts the records in the database using the given statement.
// window determines the batch size.
func putRecords(ctx context.Context, db *sql.DB, statement string, window int, recs <-chan []interface{}, num int) <-chan int {
	out := make(chan int, 1)
	var processed int
	go func() {
		var i int
		var tx *sql.Tx
		log.Printf("putRecords: started goroutine %d", num)
		defer close(out)
		for parms := range recs {
			if i%window == 0 {
				if tx != nil {
					_ = tx.Commit()
				}
				t, err := db.Begin()
				if err != nil {
					log.Printf("putRecords: goroutine %d: error starting transaction: %s", num, err)
					return
				}
				tx = t
			}
			_, err := tx.Exec(statement, parms...)
			if err != nil {
				log.Printf("putRecords: goroutine %d: error executing statement: %s", num, err)
				return
			}
			i++
			// Stop if done.
			select {
			case <-ctx.Done():
				log.Printf("putRecords: done closed")
				return
			default:
			}
			processed++
		}
		if tx != nil {
			_ = tx.Commit()
		}
		out <- processed
		log.Printf("putRecords: goroutine %d: all parms processed, sending %d results", num, processed)
		log.Printf("putRecords: end of goroutine %d", num)
	}()
	return out
}

func buildInsertStatement(table string, cols []field.Converter) string {
	w := &bytes.Buffer{}
	fmt.Fprintf(w, "insert into %s (", table)
	for i, col := range cols {
		if i > 0 {
			fmt.Fprintf(w, ",")
		}
		fmt.Fprintf(w, "%s", col.Name)
	}
	fmt.Fprint(w, ") values (")
	for i := 0; i < len(cols); i++ {
		if i > 0 {
			fmt.Fprintf(w, ",")
		}
		fmt.Fprintf(w, "$%d", i+1)
	}
	fmt.Fprint(w, ");")
	return w.String()
}

type options struct {
	inputFiles []string
	dbHost     string
	dbName     string
	user       string
	password   string
	prompt     bool
	tablename  string
	statement  string
	parallel   int
}

func (o *options) Parse(args []string) {
	f := pflag.NewFlagSet("default", pflag.ContinueOnError)
	f.StringArrayVarP(&o.inputFiles, "input", "i", []string{}, "names of the files to load")
	f.StringVarP(&o.dbHost, "host", "h", "localhost", "db server host")
	f.StringVarP(&o.dbName, "dbname", "d", "azurecosts", "name of the database")
	f.StringVarP(&o.tablename, "table", "t", "azurecosts", "name of the database table")
	f.StringVarP(&o.user, "user", "u", "peza", "username")
	f.StringVarP(&o.password, "password", "p", "", "password")
	f.IntVarP(&o.parallel, "procs", "n", -1, "number of parallel processes. Defaults to number of available logical processors")
	f.BoolVarP(&o.prompt, "prompt", "P", false, "prompt for password")
	_ = f.Parse(args[1:])

	o.statement = buildInsertStatement(o.tablename, converters)
	if o.parallel <= 0 {
		o.parallel = runtime.NumCPU()
	}
}

func setPassword(opts *options) error {
	if !opts.prompt {
		return nil
	}
	fmt.Print("Password: ")
	b, err := terminal.ReadPassword(0)
	if err != nil {
		return err
	}
	fmt.Println()
	opts.password = string(b)
	return nil
}

func testConnection(db *sql.DB) error {
	res, err := db.Query(`select * from public.azurecosts`)
	if err != nil {
		return err
	}
	_ = res
	return nil
}

const (
	connStr = "user=peza password=password dbname=azurecosts host=localhost sslmode=disable"
	connFmt = "user='%s' password='%s' dbname='%s' host='%s' sslmode=disable"
)

func connectionString(user, password, dbname, host string) string {
	return fmt.Sprintf(connFmt, user, password, dbname, host)
}

func openDB(opts *options) (*sql.DB, error) {
	db, err := sql.Open("postgres",
		connectionString(
			opts.user,
			opts.password,
			opts.dbName,
			opts.dbHost,
		))
	if err != nil {
		return nil, err
	}
	return db, nil
}

type contextValue string

var (
	ctxValueLogger = contextValue("logger")
)

func contextLogger(ctx context.Context) *log.Logger {
	l, ok := ctx.Value(ctxValueLogger).(*log.Logger)
	if !ok {
		return nil
	}
	return l
}

func newLogger() *log.Logger {
	l := log.New(os.Stderr, "", log.LstdFlags)
	return l
}

type config struct {
	db *sql.DB
}

var DefaultConfig config

func mergeInt(ctx context.Context, cs ...<-chan int) <-chan int {
	var wg sync.WaitGroup
	out := make(chan int)

	// Start an output goroutine for each input channel in cs.  output
	// copies values from c to out until c is closed, then calls wg.Done.
	output := func(c <-chan int) {
		for n := range c {
			out <- n
		}
		wg.Done()
	}
	wg.Add(len(cs))
	for _, c := range cs {
		go output(c)
	}

	// Start a goroutine to close out once all the output goroutines are
	// done.  This must start after the wg.Add call.
	go func() {
		wg.Wait()
		close(out)
	}()
	return out
}

func reduceInt(ctx context.Context, ints <-chan int, f func(acc, i int) int) <-chan int {
	out := make(chan int, 1)
	go func() {
		defer close(out)
		var res int
		for i := range ints {
			res = f(res, i)
		}
		out <- res
	}()
	return out
}

func main() {
	options := &options{}
	options.Parse(os.Args)
	if len(options.inputFiles) == 0 {
		fmt.Println("input files missing, use --input per file.")
		return
	}

	// set password
	if err := setPassword(options); err != nil {
		return
	}

	// Open database.
	var err error
	DefaultConfig.db, err = openDB(options)
	if err != nil {
		fmt.Printf("error opening database: %s\n", err)
		return
	}
	// test the database
	if err := testConnection(DefaultConfig.db); err != nil {
		fmt.Printf("error connecting to the database: %s\n", err)
		return
	}
	defer DefaultConfig.db.Close()

	log := newLogger()
	ctx, cancel := context.WithCancel(context.WithValue(context.Background(), ctxValueLogger, log))
	defer cancel()
	namesChan := stringChan(ctx, options.inputFiles...)
	filesChan := fileChan(ctx, namesChan)
	recsChan := recordChanFromReader(ctx, filesChan)
	valsChan := convertRecord(ctx, recsChan)

	// Build array of res channels and processes.
	resChans := make([]<-chan int, options.parallel)
	for i := range resChans {
		resChans[i] = putRecords(ctx, DefaultConfig.db, options.statement, 100, valsChan, i)
	}
	// Merg the results.
	resChan := mergeInt(ctx, resChans...)
	// Add the results.
	resChan = reduceInt(ctx, resChan,
		func(acc, i int) int {
			return acc + i
		})

	// Set up ctrl c abort.
	signalChan := make(chan os.Signal, 1)

	var notDone = true
	for notDone {
		select {
		case res := <-resChan:
			fmt.Printf("imported %d records\n", res)
			notDone = false
		case res := <-signalChan:
			fmt.Printf("caught signal: %s\n", res.String())
			notDone = false
		case <-time.Tick(time.Second):
		}
	}
}
