module azurecosts

go 1.15

require (
	github.com/lib/pq v1.8.0
	github.com/spf13/pflag v1.0.5
	golang.org/x/crypto v0.0.0-20201002170205-7f63de1d35b0
)
