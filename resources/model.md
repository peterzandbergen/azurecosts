| Column name | Type | SQL |
|-------------|------|-----|
| BillingAccountId| Text  | BillingAccountId character varying(500) COLLATE pg_catalog."default"    |
| BillingAccountName| Text  | BillingAccountName character varying(500) COLLATE pg_catalog."default"    |
| BillingPeriodStartDate| Date  |  BillingPeriodStartDate  date   |
| BillingPeriodEndDate| Date  |  BillingPeriodEndDate date   |
| BillingProfileId| Text  | BillingProfileId character varying(500) COLLATE pg_catalog."default"    |
| BillingProfileName| Text  | BillingProfileName character varying(500) COLLATE pg_catalog."default"    |
| AccountOwnerId| Text  | AccountOwnerId character varying(500) COLLATE pg_catalog."default"    |
| AccountName| Text  | AccountName character varying(500) COLLATE pg_catalog."default"    |
| SubscriptionId| Text  | SubscriptionId character varying(500) COLLATE pg_catalog."default"    |
| SubscriptionName| Text  | SubscriptionName character varying(500) COLLATE pg_catalog."default"    |
| Date| Date  |  Date date   |
| Product| Text  | Product character varying(500) COLLATE pg_catalog."default"    |
| PartNumber| Text  | PartNumber character varying(500) COLLATE pg_catalog."default"    |
| MeterId| Text  | MeterId character varying(500) COLLATE pg_catalog."default"    |
| ServiceFamily| Text  | ServiceFamily character varying(500) COLLATE pg_catalog."default"    |
| MeterCategory| Text  | MeterCategory character varying(500) COLLATE pg_catalog."default"    |
| MeterSubCategory| Text  | MeterSubCategory character varying(500) COLLATE pg_catalog."default"    |
| MeterRegion| Text  | MeterRegion character varying(500) COLLATE pg_catalog."default"    |
| MeterName| Text  | MeterName character varying(500) COLLATE pg_catalog."default"    |
| Quantity| Numeric  |  Quantity numeric(40, 30)   |
| EffectivePrice| Numeric  |  EffectivePrice numeric(40, 30)   |
| Cost| Numeric  |  Cost numeric(40, 30)   |
| UnitPrice| Numeric  |  UnitPrice numeric(40, 30)   |
| BillingCurrency| Text  | BillingCurrency character varying(500) COLLATE pg_catalog."default"    |
| ResourceLocation| Text  | ResourceLocation character varying(500) COLLATE pg_catalog."default"    |
| AvailabilityZone| Text  | AvailabilityZone character varying(500) COLLATE pg_catalog."default"    |
| ConsumedService| Text  | ConsumedService character varying(500) COLLATE pg_catalog."default"    |
| ResourceId| Text  | ResourceId character varying(500) COLLATE pg_catalog."default"    |
| ResourceName| Text  | ResourceName character varying(500) COLLATE pg_catalog."default"    |
| ServiceInfo1| Text  | ServiceInfo1 character varying(500) COLLATE pg_catalog."default"    |
| ServiceInfo2| Text  | ServiceInfo2 character varying(500) COLLATE pg_catalog."default"    |
| AdditionalInfo| JSON  |  AdditionalInfo json   |
| Tags| Text  | Tags character varying(500) COLLATE pg_catalog."default"    |
| InvoiceSectionId| Text  | InvoiceSectionId character varying(500) COLLATE pg_catalog."default"    |
| InvoiceSection| Text  | InvoiceSection character varying(500) COLLATE pg_catalog."default"    |
| CostCenter| Text  | CostCenter character varying(500) COLLATE pg_catalog."default"    |
| UnitOfMeasure| Text  | UnitOfMeasure character varying(500) COLLATE pg_catalog."default"    |
| ResourceGroup| Text  | ResourceGroup character varying(500) COLLATE pg_catalog."default"    |
| ReservationId| Text  | ReservationId character varying(500) COLLATE pg_catalog."default"    |
| ReservationName| Text  | ReservationName character varying(500) COLLATE pg_catalog."default"    |
| ProductOrderId| Text  | ProductOrderId character varying(500) COLLATE pg_catalog."default"    |
| ProductOrderName| Text  | ProductOrderName character varying(500) COLLATE pg_catalog."default"    |
| OfferId| Text  | OfferId character varying(500) COLLATE pg_catalog."default"    |
| IsAzureCreditEligible| Bool  |     |
| Term| Text  | Term character varying(500) COLLATE pg_catalog."default"    |
| PublisherName| Text  | PublisherName character varying(500) COLLATE pg_catalog."default"    |
| PlanName| Text  | PlanName character varying(500) COLLATE pg_catalog."default"    |
| ChargeType| Text  | ChargeType character varying(500) COLLATE pg_catalog."default"    |
| Frequency| Text  | Frequency character varying(500) COLLATE pg_catalog."default"    |
| PublisherType| Text  | PublisherType character varying(500) COLLATE pg_catalog."default"    |
| PayGPrice| Numeric  |  PayGPrice numeric(40, 30)   |
| PricingModel| Text  | PricingModel character varying(500) COLLATE pg_catalog."default"    |
