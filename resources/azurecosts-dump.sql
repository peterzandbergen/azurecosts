--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2 (Debian 12.2-2.pgdg100+1)
-- Dumped by pg_dump version 12.2 (Debian 12.2-2.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: azurecosts; Type: DATABASE; Schema: -; Owner: admin
--

CREATE DATABASE azurecosts WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';


ALTER DATABASE azurecosts OWNER TO admin;

\connect azurecosts

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: azurecosts; Type: TABLE; Schema: public; Owner: peza
--

CREATE TABLE public.azurecosts (
    billingaccountid character varying(500),
    billingaccountname character varying(500),
    billingperiodstartdate date,
    billingperiodenddate date,
    billingprofileid character varying(500),
    billingprofilename character varying(500),
    accountownerid character varying(500),
    accountname character varying(500),
    subscriptionid character varying(500),
    subscriptionname character varying(500),
    date date,
    product character varying(500),
    partnumber character varying(500),
    meterid character varying(500),
    servicefamily character varying(500),
    metercategory character varying(500),
    metersubcategory character varying(500),
    meterregion character varying(500),
    metername character varying(500),
    quantity numeric(40,30),
    effectiveprice numeric(40,30),
    cost numeric(40,30),
    unitprice numeric(40,30),
    billingcurrency character varying(500),
    resourcelocation character varying(500),
    availabilityzone character varying(500),
    consumedservice character varying(500),
    resourceid character varying(500),
    resourcename character varying(500),
    serviceinfo1 character varying(500),
    serviceinfo2 character varying(500),
    additionalinfo json,
    tags character varying(500),
    invoicesectionid character varying(500),
    invoicesection character varying(500),
    costcenter character varying(500),
    unitofmeasure character varying(500),
    resourcegroup character varying(500),
    reservationid character varying(500),
    reservationname character varying(500),
    productorderid character varying(500),
    productordername character varying(500),
    offerid character varying(500),
    isazurecrediteligible boolean,
    term character varying(500),
    publishername character varying(500),
    planname character varying(500),
    chargetype character varying(500),
    frequency character varying(500),
    publishertype character varying(500),
    paygprice numeric(40,30),
    pricingmodel character varying(500)
);


ALTER TABLE public.azurecosts OWNER TO peza;

--
-- PostgreSQL database dump complete
--

