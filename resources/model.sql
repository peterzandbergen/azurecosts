-- Table: public.azurecosts

-- DROP TABLE public.azurecosts;

CREATE TABLE public.azurecosts
(
    billingaccountid character varying(500) COLLATE pg_catalog."default",
    billingaccountname character varying(500) COLLATE pg_catalog."default",
    billingperiodstartdate date,
    billingperiodenddate date,
    billingprofileid character varying(500) COLLATE pg_catalog."default",
    billingprofilename character varying(500) COLLATE pg_catalog."default",
    accountownerid character varying(500) COLLATE pg_catalog."default",
    accountname character varying(500) COLLATE pg_catalog."default",
    subscriptionid character varying(500) COLLATE pg_catalog."default",
    subscriptionname character varying(500) COLLATE pg_catalog."default",
    date date,
    product character varying(500) COLLATE pg_catalog."default",
    partnumber character varying(500) COLLATE pg_catalog."default",
    meterid character varying(500) COLLATE pg_catalog."default",
    servicefamily character varying(500) COLLATE pg_catalog."default",
    metercategory character varying(500) COLLATE pg_catalog."default",
    metersubcategory character varying(500) COLLATE pg_catalog."default",
    meterregion character varying(500) COLLATE pg_catalog."default",
    metername character varying(500) COLLATE pg_catalog."default",
    quantity numeric(40,30),
    effectiveprice numeric(40,30),
    cost numeric(40,30),
    unitprice numeric(40,30),
    billingcurrency character varying(500) COLLATE pg_catalog."default",
    resourcelocation character varying(500) COLLATE pg_catalog."default",
    availabilityzone character varying(500) COLLATE pg_catalog."default",
    consumedservice character varying(500) COLLATE pg_catalog."default",
    resourceid character varying(500) COLLATE pg_catalog."default",
    resourcename character varying(500) COLLATE pg_catalog."default",
    serviceinfo1 character varying(500) COLLATE pg_catalog."default",
    serviceinfo2 character varying(500) COLLATE pg_catalog."default",
    additionalinfo json,
    tags character varying(500) COLLATE pg_catalog."default",
    invoicesectionid character varying(500) COLLATE pg_catalog."default",
    invoicesection character varying(500) COLLATE pg_catalog."default",
    costcenter character varying(500) COLLATE pg_catalog."default",
    unitofmeasure character varying(500) COLLATE pg_catalog."default",
    resourcegroup character varying(500) COLLATE pg_catalog."default",
    reservationid character varying(500) COLLATE pg_catalog."default",
    reservationname character varying(500) COLLATE pg_catalog."default",
    productorderid character varying(500) COLLATE pg_catalog."default",
    productordername character varying(500) COLLATE pg_catalog."default",
    offerid character varying(500) COLLATE pg_catalog."default",
    isazurecrediteligible boolean,
    term character varying(500) COLLATE pg_catalog."default",
    publishername character varying(500) COLLATE pg_catalog."default",
    planname character varying(500) COLLATE pg_catalog."default",
    chargetype character varying(500) COLLATE pg_catalog."default",
    frequency character varying(500) COLLATE pg_catalog."default",
    publishertype character varying(500) COLLATE pg_catalog."default",
    paygprice numeric(40,30),
    pricingmodel character varying(500) COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE public.azurecosts
    OWNER to peza;