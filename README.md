# Azure Costs

Program to import detailed Azure Cost reports into Postgres (for now)

## Backup and restore

Backup the database as follows:

```
pg_dump --file=azurecosts-dump.sql --user=peza --sc--create  azurecosts
```

## Command to load the files

```
go run cmd/loadfilesp/main.go \
    --password \
    --user peza \
    --input "/mnt/c/Users/peza/OneDrive/CBS/FinOps/Detail_Enrollment_83856065_202001_en.csv" \
    --input "/mnt/c/Users/peza/OneDrive/CBS/FinOps/Detail_Enrollment_83856065_202002_en.csv" \
    --input "/mnt/c/Users/peza/OneDrive/CBS/FinOps/Detail_Enrollment_83856065_202003_en.csv" \
    --input "/mnt/c/Users/peza/OneDrive/CBS/FinOps/Detail_Enrollment_83856065_202004_en.csv" \
    --input "/mnt/c/Users/peza/OneDrive/CBS/FinOps/Detail_Enrollment_83856065_202005_en.csv" \
    --input "/mnt/c/Users/peza/OneDrive/CBS/FinOps/Detail_Enrollment_83856065_202006_en.csv" \
    --input "/mnt/c/Users/peza/OneDrive/CBS/FinOps/Detail_Enrollment_83856065_202007_en.csv" \
    --input "/mnt/c/Users/peza/OneDrive/CBS/FinOps/Detail_Enrollment_83856065_202008_en.csv" \
    --input "/mnt/c/Users/peza/OneDrive/CBS/FinOps/Detail_Enrollment_83856065_202009_en.csv" 
```

