package field

import (
	"encoding/json"
	"strings"
	"time"
)

type ConverterFunc func(string) interface{}

type Converter struct {
	Name string
	Conv ConverterFunc
}

func StringConverter(val string) interface{} {
	return val
}

func BooleanConverter(val string) interface{} {
	return strings.ToLower(val) == "true"
}

type JSONField map[string]interface{}

func JSONConverter(val string) interface{} {
	var j JSONField
	if err := json.Unmarshal([]byte(val), &j); err != nil {
		return nil
	}
	return j
}

func NewDateConverter(format string) ConverterFunc {
	return func(val string) interface{} {
		res, err := time.Parse(format, val)
		if err != nil {
			return nil
		}
		return res
	}
}
